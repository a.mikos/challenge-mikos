*** Settings ***
Library    RequestsLibrary
Library    Collections
Library    String
Library    SeleniumLibrary
Library    XML


***Variable***
${base_url}             http://api.exchangeratesapi.io/v1
    
***Keywords***
access API and convert currency
    Create Session    Challenge    ${base_url}
    ${data}=    Create Dictionary   
    ${​RESPONSE}=      Get Request     Challenge    /latest?access_key=0cfd9b9bdd309d8ff655278802f28f09&base&symbols=   json=${data}    
    Should Be Equal As Strings    ${​RESPONSE.status_code}    200  
    ${retornoBRL}=       Set Variable       ${​RESPONSE.json()['rates']['BRL']} 

    #Value Conversion
    ${productValue}=        Get Text             ${amazon.itemValue} 
    ${productValue}=        Remove String        ${productValue}      R$
    ${productValue}=        Remove String        ${productValue}      ,

    ${Euro}=               Evaluate              ${productValue} / ${retornoBRL}
    ${ConvertEuro}=        Convert To Number     ${Euro}    3
    ${FormatEuro}=         Split String          ${ConvertEuro},00
    Log To Console     ${FormatEuro}


    

