*** Settings ***
Documentation           Functionality: Challenge 
Resource               ../resource/main.resource
Library    SeleniumLibrary

*** Variable ***


*** Keywords ******
since it's on the google page
  Go To                             ${URLgoogle} 
  Wait Until Element Is Visible     ${google.searchGoogle}

search for Amazon Brazil
  Click Element                     ${google.searchGoogle}
  Input Text                        ${google.searchGoogle}     Amazon Brasil
  Press Keys         None           ENTER



click on the link
   Wait Until Element Is Visible     ${google.amazonLink}
   Click Element                     ${google.amazonLink}
  