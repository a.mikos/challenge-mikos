*** Settings ***
Documentation          Functionality: Challenge 
Resource               ../resource/main.resource
Library                SeleniumLibrary

*** Variable ***


*** Keywords ******
that is on the website of Amazon BR
   Go To                          ${URLamz}
   Title Should Be                Amazon.com.br | Compre livros, Kindle, Echo, Fire Tv e mais.

search for the term iPhone
   Click Element                  ${amazon.searchAmz}
   Input Text                     ${amazon.searchAmz}     iPhone
   Press Keys        None         ENTER
   Sleep    3s

count items starting with this term
   Get Count    //a[starts-with(.,'iPhone')]    iPhone
   ${count}   Get Element Count     //a[starts-with(.,'iPhone')] 
   Log To Console     ${count}
   
present the highest value item
   Click Element    ${amazon.highlights}
   Click Element    ${amazon.highPrice}
   Wait Until Element Is Visible    //div[@class='s-desktop-content s-opposite-dir sg-row']
   Sleep    2

on the highest valued item
   ${preco}=     Get Text        ${amazon.bigPrice}   

convert currency
   access API and convert currency

click on the highest valued item
   Click Element       ${amazon.highValue} 
   Wait Until Element Is Visible    ${amazon.itemValue} 

search Amazon for the most valuable term - iPhone
   that is on the website of Amazon BR
   search for the term iPhone