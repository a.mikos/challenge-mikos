*** Settings ***
Documentation    Functionality: Challenge 
Resource         ../resource/main.resource
Resource         ../resource/BDD.robot
Test Setup       Open the Browser
Test Teardown    Close the Browser

*** Variable ***

*** Test Case ***
Scenario 01: Search Amazon Brazil
   Given     since it's on the google page
   When      search for Amazon Brazil
   Then       click on the link   

Scenario 02: Search Iphone and Count Items
   Given     that is on the website of Amazon BR
   When      search for the term iPhone
   Then      count items starting with this term 

Scenario 03: Take the most expensive item on the page and convert it to EUR
   Given     search Amazon for the most valuable term - iPhone
   When      present the highest value item
   And       click on the highest valued item
   Then      convert currency      

